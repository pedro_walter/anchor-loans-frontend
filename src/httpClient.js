import axios from "axios";

import { API_URL, AUTH_TOKEN_LOCAL_STORAGE_KEY } from "./constants";

const instance = axios.create({
  baseURL: API_URL,
});
instance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem(AUTH_TOKEN_LOCAL_STORAGE_KEY);
    if (token) {
      config.headers.Authorization = `Token ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

export default instance;
