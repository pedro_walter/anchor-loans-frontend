import Vue from "vue";
import { AUTH_TOKEN_LOCAL_STORAGE_KEY } from "./constants";
import axios from "./httpClient";

async function getUserData() {
  if (localStorage.getItem(AUTH_TOKEN_LOCAL_STORAGE_KEY) !== null) {
    const response = await axios.get("/rest-auth/user/");
    return response.data;
  } else return {};
}

const store = Vue.observable({
  debug: true,
  registrationSuccesful: false,
  authToken: localStorage.getItem(AUTH_TOKEN_LOCAL_STORAGE_KEY),
  userData: {},
  setRegistrationSuccesful(newValue) {
    if (this.debug)
      console.log("setRegistrationSuccesful triggered with ", newValue);
    this.registrationSuccesful = newValue;
  },
  async updateAuthToken(newToken) {
    if (this.debug) console.log("updateAuthToken triggered with ", newToken);
    localStorage.setItem(AUTH_TOKEN_LOCAL_STORAGE_KEY, newToken);
    this.authToken = newToken;
    this.userData = await getUserData();
  },
  clearAuthToken() {
    if (this.debug) console.log("clearAuthToken triggered");
    this.authToken = null;
    localStorage.removeItem(AUTH_TOKEN_LOCAL_STORAGE_KEY);
  },
});

getUserData()
  .then((response) => {
    store.userData = response;
  })
  .catch((error) => {
    if (error.response.status === 401) {
      store.clearAuthToken();
    }
  });

export default store;
