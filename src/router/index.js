import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
  {
    path: "/pending",
    name: "Photos Pending Approval",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "pending" */ "../views/Pending.vue"),
  },
  {
    path: "/register",
    name: "Register an account",
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/Register.vue"),
  },
  {
    path: "/validate-account",
    name: "Confirm the registration of an account",
    component: () =>
      import(
        /* webpackChunkName: "validateAccount" */ "../views/ValidateAccount.vue"
      ),
  },
  {
    path: "/validate-account/:token",
    props: true,
    name: "Confirm the registration of an account from link in email",
    component: () =>
      import(
        /* webpackChunkName: "validateAccount" */ "../views/ValidateAccount.vue"
      ),
  },
  {
    path: "/upload",
    props: true,
    name: "Upload a picture",
    component: () =>
      import(/* webpackChunkName: "uploadPhoto" */ "../views/UploadPhoto.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
