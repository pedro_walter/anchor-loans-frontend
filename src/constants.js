let API_URL = "API_URL_REPLACE_ME";
if (process.env.NODE_ENV === "development") {
  // This way we can pass params to static files. see Dockerfile.
  // when build default env is production
  API_URL = process.env.VUE_APP_API_URL;
}
export const AUTH_TOKEN_LOCAL_STORAGE_KEY = "authToken";
export const PHOTO_STATUS = {
  PENDING_APPROVAL: 1,
  APPROVED: 2,
  DENIED: 3,
};
export {
  API_URL
}